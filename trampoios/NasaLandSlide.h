//
//  NasaLandSlide.h
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>

@interface NasaLandSlide : JSONModel

@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (strong, nonatomic) NSString *source_lin;

typedef void (^HandlerSuccessLandSlide)(NSArray *responseObject);
typedef void (^HandlerFailLandSlide)(NSError *error);

-(void)getLandSlideEvents:(HandlerSuccessLandSlide)success fail:(HandlerFailLandSlide)fail;

@end
