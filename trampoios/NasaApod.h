//
//  NasaApod.h
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>

@interface NasaApod : JSONModel

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *explanation;
@property (strong, nonatomic) NSString *url;

typedef void (^HandlerSuccessApod)(NasaApod *responseObject);
typedef void (^HandlerFailApod)(NSError *error);

-(void)getPictureOfDay:(NSDate *)date successBlock:(HandlerSuccessApod)success failBlock:(HandlerFailApod)fail;

@end
