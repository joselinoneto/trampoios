//
//  NasaSounds.h
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>

@interface NasaSounds : JSONModel

//@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *stream_url;
@property (assign, nonatomic) NSInteger duration;

typedef void (^HandlerSuccessSounds)(NSArray *responseObject);
typedef void (^HandlerFailSounds)(NSError *error);

-(void)getSoundsFromSpace:(HandlerSuccessSounds)success fail:(HandlerFailSounds)fail;

@end
