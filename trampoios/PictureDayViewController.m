//
//  PictureDayViewController.m
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import "PictureDayViewController.h"
#import "JTCalendar.h"
#import "NasaApod.h"

@interface PictureDayViewController () <JTCalendarDelegate>
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *topMenu;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *horizontalMenu;
@property (weak, nonatomic) IBOutlet UIImageView *pictureView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (strong, nonatomic) NSDate *dateSelected;


@end

@implementation PictureDayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Configurar JTCalendar
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    [_calendarManager setMenuView:_topMenu];
    [_calendarManager setContentView:_horizontalMenu];
    [_calendarManager setDate:[NSDate date]];
    
    _calendarManager.settings.weekModeEnabled = YES;
    [_calendarManager reload];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - JTCalendarDelegate
- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    // Use to indicate the selected date
    _dateSelected = dayView.date;
    //[self atualizarLocalComDataInicial:_dateSelected];
    
    NasaApod *apod = [[NasaApod alloc] init];
    [apod getPictureOfDay:_dateSelected successBlock:^(NasaApod *responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [NSURL URLWithString:responseObject.url];
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            UIImage *imagem = [[UIImage alloc] initWithData:imageData];
            self.pictureView.image = imagem;
        });
    } failBlock:^(NSError *error) {
        
    }];
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    // Load the previous or next page if touch a day from another month
    if(![_calendarManager.dateHelper date:_horizontalMenu.date isTheSameMonthThan:dayView.date]){
        if([_horizontalMenu.date compare:dayView.date] == NSOrderedAscending){
            [_horizontalMenu loadNextPageWithAnimation];
        }
        else{
            [_horizontalMenu loadPreviousPageWithAnimation];
        }
    }
}

- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    
    // Test if the dayView is from another month than the page
    // Use only in month mode for indicate the day of the previous or next month
    if([dayView isFromAnotherMonth]){
        dayView.hidden = YES;
    }
    // Today
    else if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
}


@end
