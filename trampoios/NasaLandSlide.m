//
//  NasaLandSlide.m
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import "NasaLandSlide.h"
#import <AFNetworking.h>
#define NASA_URL @"https://data.nasa.gov/resource/9ns5-uuif.json"

@implementation NasaLandSlide

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}


-(void)getLandSlideEvents:(HandlerSuccessLandSlide)success fail:(HandlerFailLandSlide)fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:NASA_URL parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *erro;
        NSLog(@"%@", responseObject);
        NSArray *vetor = [NasaLandSlide arrayOfModelsFromDictionaries:responseObject error:&erro];
        success(vetor);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        fail(error);
    }];
}

@end
