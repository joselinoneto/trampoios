//
//  NasaSounds.m
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import "NasaSounds.h"
#import <AFNetworking.h>

#define NASA_API_KEY @"&api_key=L5RXx9ScP4wceoyeZVi5OW0f8iiXSrpRqYgQ2PvZ"
#define NASA_URL_SOUND_SPACE @"https://api.nasa.gov/planetary/sounds?q=apollo"
#define SOUND_CLOUD_API @"?client_id=8e87656ca1b975b9049764bc420c2b6f"

@implementation NasaSounds

-(NSString *)stream_url{
    return [_stream_url stringByAppendingString:SOUND_CLOUD_API];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

-(void)getSoundsFromSpace:(HandlerSuccessSounds)success fail:(HandlerFailSounds)fail{
    NSString *urlFinal = [NASA_URL_SOUND_SPACE stringByAppendingString:NASA_API_KEY];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:urlFinal parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *erro;
        NSLog(@"%@", responseObject);
        NSArray *vetor = [NasaSounds arrayOfModelsFromDictionaries:[responseObject objectForKey:@"results"] error:&erro];
        success(vetor);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        fail(error);
    }];
}

@end
