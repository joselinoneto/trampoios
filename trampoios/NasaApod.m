//
//  NasaApod.m
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import "NasaApod.h"
#import <AFNetworking.h>

#define NASA_API_KEY @"&api_key=L5RXx9ScP4wceoyeZVi5OW0f8iiXSrpRqYgQ2PvZ"
#define NASA_URL @"https://api.nasa.gov/planetary/apod?date="

@implementation NasaApod

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

-(void)getPictureOfDay:(NSDate *)date successBlock:(HandlerSuccessApod)success failBlock:(HandlerFailApod)fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *finallUrl = NASA_URL;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    NSString *year = [NSString stringWithFormat:@"%ld", [components year]];
    NSString *month = [NSString stringWithFormat:@"%ld", [components month]];
    NSString *day = [NSString stringWithFormat:@"%ld", [components day]];
    
    finallUrl = [finallUrl stringByAppendingString:year];
    finallUrl = [finallUrl stringByAppendingString:@"-"];
    finallUrl = [finallUrl stringByAppendingString:month];
    finallUrl = [finallUrl stringByAppendingString:@"-"];
    finallUrl = [finallUrl stringByAppendingString:day];
    finallUrl = [finallUrl stringByAppendingString:NASA_API_KEY];
    
    [manager GET:finallUrl parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *erro;
        NSLog(@"%@", responseObject);
        NasaApod *apod = [[NasaApod alloc] initWithDictionary:responseObject error:&erro];
        success(apod);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        fail(error);
    }];
}

@end
