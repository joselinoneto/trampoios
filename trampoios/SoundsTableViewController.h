//
//  SoundsTableViewController.h
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoundsTableViewController : UITableViewController <UITableViewDelegate>

@end
