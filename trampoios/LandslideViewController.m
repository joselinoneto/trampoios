//
//  LandslideViewController.m
//  trampoios
//
//  Created by Jose Lino Neto on 6/22/16.
//  Copyright © 2016 Construtor. All rights reserved.
//

#import "LandslideViewController.h"
#import "NasaLandSlide.h"
@import GoogleMaps;

@interface LandslideViewController () <GMSMapViewDelegate, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapa;
@property (nonatomic, retain) UIWebView *myWebView;

@end

@implementation LandslideViewController

-(GMSMapView *)mapa{
    _mapa.delegate = self;
    _mapa.myLocationEnabled = YES;
    _mapa.settings.myLocationButton = YES;
    
    return _mapa;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NasaLandSlide *land = [[NasaLandSlide alloc] init];
    [land getLandSlideEvents:^(NSArray *responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Clear the markers
            [self.mapa clear];
            
            // For each marker returned from backend services includr on the map
            for (NasaLandSlide *item in responseObject) {
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.position = CLLocationCoordinate2DMake(item.latitude, item.longitude);
                //marker.title = item.title;
                //marker.snippet = item.snippet;
                marker.title = item.source_lin;
                //marker.snippet = item.snippet;
                marker.map = _mapa;
            }
        });
    } fail:^(NSError *error) {
        
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Open Map
    [self.mapa reloadInputViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:marker.title]];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
